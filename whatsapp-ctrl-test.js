/**
 * Created by mario on 06/05/15.
 */

describe('Testing sending and forwarding messages by WhatsApp', function(){

    // Suggestion for the WhatsApp data model (UNIQUE ID for conversations and messages)
    var chats = [
        {id: 1, cel: '+551188776655', current: false, messages: [
            {id: 1, msg: 'Loren ipsum 1', selected: false},
            {id: 2, msg: 'Loren ipsum 2', selected: true}
        ]},
        {id: 2, cel: '+552199556633', current: true, messages: [
            {id: 3, msg: 'Loren ipsum 3', selected: false},
            {id: 4, msg: 'Loren ipsum 4', selected: false}
        ]}
    ];

    var msgs = [
        {id: 1, msg: 'Loren ipsum 1', selected: false},
        {id: 2, msg: 'Loren ipsum 2', selected: true},
        {id: 3, msg: 'Loren ipsum 3', selected: false},
        {id: 4, msg: 'Loren ipsum 4', selected: false}
    ];

    var whatsAppCtrl, sendMessageObj, mockBackend;
    beforeEach(module('whatsApp'));

    beforeEach(inject(function($controller, $httpBackend){
        mockBackend = $httpBackend;

        // Get conversations
        mockBackend.expectGET('/api/conversations')
            .respond(chats);
        // Get messages
        mockBackend.expectGET('/api/messages')
            .respond(msgs);
        // Get a message item by id
        mockBackend.expectGET('/api/messages/1')
            .respond(msgs[0]);

        whatsAppCtrl = $controller('WhatsAppCtrl');
        // Insert message to be sent in the message field
        whatsAppCtrl.messageField = 'Loren ipsum 5';
        // Run the message sending function to the message list
        sendMessageObj = whatsAppCtrl.sendMessage(); // Send and return message object
    }));

    it('Should load conversation items from server', function(){
        expect(whatsAppCtrl.conversations).toEqual([]);
        mockBackend.flush(); // Simulate request to the server
        expect(whatsAppCtrl.conversations).toEqual(chats);
    });

    it('Should load just message items from server', function(){
        expect(whatsAppCtrl.messages).toEqual([]);
        mockBackend.flush(); // Simulate request to the server
        expect(whatsAppCtrl.messages).toEqual(msgs);
    });

    it('Should load just a message item from server', function(){
        expect(whatsAppCtrl.getMessage(1)).toEqual({});
        mockBackend.flush(); // Simulate request to the server
        expect(whatsAppCtrl.getMessage(1)).toEqual(msgs[0]);
    });

    it('Should perform the sending of the message', function(){
        expect(whatsAppCtrl.conversations).toEqual([]);
        mockBackend.flush(); // Simulate request to the server
        whatsAppCtrl.conversations.forEach(function(obj){
            if (obj.current) {
                obj.messages.forEach(function(message){
                    if (message.msg === whatsAppCtrl.messageField) {
                        // Check if the message was sent to the current conversation by its id
                        expect(sendMessageObj.id).toEqual(message.id);
                    }
                });
                var lastMessageOfCurrConversation = obj.messages.pop();
                // Check if the message is sent as the last message in the message list of the current conversation
                expect(sendMessageObj.id).toEqual(lastMessageOfCurrConversation.id);
            }
        });
    });

    it('Should be selected the message to be forwarded to another conversation', function(){
        whatsAppCtrl.selectMessage(1); // Pass the message ID to the function
        expect(whatsAppCtrl.conversations).toEqual([]);
        mockBackend.flush(); // Simulate request to the server
        whatsAppCtrl.conversations.forEach(function(obj){
            if (obj.id === 1) {
                obj.messages.forEach(function(message){
                    if (message.id === 1) {
                        expect(message.selected).toBeTruthy();
                    } else {
                        expect(message.selected).toBeFalsy();
                    }
                });
            } else {
                obj.messages.forEach(function(message){
                    expect(message.selected).toBeFalsy();
                });
            }
        });
    });

    it('Should forward the selected message to another conversation', function(){
        // Selected the message to be forwarded to another conversation
        whatsAppCtrl.selectMessage(1);
        // Perform message forwarding function passing as parameter the id of the conversation that will be sent
        whatsAppCtrl.messageFowarding(2);
        // Check if the attribute "current" in the current conversation is "true" and check if the attribute "current"
        // of other conversations are "false"
        var chat2; // Variable used to compare the messages
        expect(whatsAppCtrl.conversations).toEqual([]);
        mockBackend.flush(); // Simulate request to the server
        whatsAppCtrl.conversations.forEach(function(obj){
            if (obj.id === 2) {
                chat2 = obj;
                expect(chat2.current).toBeTruthy();
            } else {
                expect(obj.current).toBeFalsy();
            }
        });
        // Check if the forwarded message is the same that was copied
        expect(whatsAppCtrl.getMessage(1)).toEqual({});
        mockBackend.flush(); // Simulate request to the server
        expect(chat2.messages.slice(-1)[0]).toEqual(whatsAppCtrl.getMessage(1));
    });
});